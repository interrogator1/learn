# Learning *git*/*GitLab*

> This repository is a shared space, in which we can learn to use the basics of git and GitLab.

## What is git?

git is a protocol, or command line tool, used for *distributed version control*. It is accessed via the command line with commands line:

```bash
git status
```

But is also wrapped up inside text editors like *Atom*. It helps track changes, authorship, rollbacks, etc. to datasets.

### What kind of datasets?

Any kind of file can be stored in a git repository, but files readable by text editor are the most common types you will find (i.e. no `.docx`, `.sql`, etc.). The prototypical use case is for writing code, but increasingly, new use cases emerge

## What is GitLab?

*GitLab* is a website, like *GitHub* or *bitbucket*, which hosts git repositories, making them viewable/editable online. Other functionality like *Issue boards* are closely associated with these sites, but are not technically a part of the git protocol itself.

UZH hosts its own instance of GitLab (https://gitlab.uzh.ch), which is accessible with Switch Drive / eduID credentials.

## Why use these tools?

Git and *GitLab* help you out when you need to keep data in an orderly state, tracking who did what to it and when. It is particularly useful whe collaborating with others, but there is no reason an invidividual research project or thesis cannot (nor should not) become a git repository!

## Why doesn't everyone use these things already?

In the world of computer programmers, they basically do. The main barrier to entry is the learning curve, which is aided by various tutorials and classes, and by graphical interfaces that limit the use of the command line.

## Plan for the session(s)

* A quick introduction
* Installations:
  * [atom](https://atom.io)
  * [git-bash](https://gitforwindows.org/) (for Windows users)
  * [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (Mac, Linux)
  * Set up git (SSH keys?)
  * Push to a repository
  * Try making a branch and getting it merged
  * Try forking and getting it merged
