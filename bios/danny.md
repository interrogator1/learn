# Danny McDonald

I'm a linguist by training, originally from Australia. I like working at the intersection of language and computing, and have used git since I was a PhD student.
